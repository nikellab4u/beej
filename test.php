<?
	$data1 = [
		'parent.child.field' => 1,
		'parent.child.field2' => 2,
		'parent2.child.name' => 'test',
		'parent2.child2.name' => 'test',
		'parent2.child2.position' => 10,
		'parent3.child3.position' => 10,
	];
	
	$data = array();
	foreach($data1 as $key=>$value){
		$elem_index = array_reverse(explode('.',$key));
		for ($i = 0; $i <= count($elem_index); $i++) {
			$key_name = $elem_index[$i];
			if($i==0){//последний ключ - присвоим значение
				unset($elem);
				$elem[$key_name] = $value;
			} elseif($i == count($elem_index)-1) {//первый ключ, сохраним результат
				$tmp_val = $elem;
				unset($elem);
				$elem[$key_name] = $tmp_val;
				unset($tmp_val);
				$data = array_merge_recursive($data,$elem);
				unset($elem);
			} else {//середина, работаем
				$tmp_val = $elem;
				unset($elem);
				$elem[$key_name] = $tmp_val;
				unset($tmp_val);
			}
			
		}

	}
	/*echo "</br>";
	echo "</br>";
	var_dump($data);*/
	
	function getMultiKey($key,$value){		
		if(!is_array($value)){
			$result[$key] = $value;
		} else {
			foreach($value as $k=>$v){				
				if(!is_array($v)){
					$result[$key.'.'.$k] = $v;					
				} else {
					$tmp = getMultiKey($key.'.'.$k, $v);					
					foreach($tmp as $k2=>$v2){
						$result[$k2] = $v2;
					}
				}
			}
		}
		return $result;
	}
	
	$res = array();
	foreach($data as $key=>$value){
			$getMultiKey = getMultiKey($key,$value);
			foreach($getMultiKey as $k=>$v){
				$res[$k] = $v;
			}
	}
	var_dump($res);
?>