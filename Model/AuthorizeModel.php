<?
require 'Model/Db.php';

class AuthorizeModel extends Model{
	public function get_data()
	{		
		if($_SESSION['admin'] == true){
			$data['authorized'] = 1;
		} else {
			$data['authorized'] = 0;
		}		
		//var_dump($params);exit;
		return $data;
	}
	
	public function authorize($params)
	{
		//var_dump($params);exit;
		if(empty($params['name'])){
			$data['error']['name'] = 'Заполните поле имя';
		}elseif ($params['name']<>'admin'){
			$data['error']['name'] = 'Неверное имя';
		}
		if(empty($params['password'])){
			$data['error']['password'] = 'Заполните поле пароль';
		} elseif ($params['password']<>'123'){
			$data['error']['password'] = 'Неверный пароль';
		}
		if(empty($data['error'])) {
			$_SESSION['admin'] = true;
			header('Location: / ');
			$data['authorized'] = 1;
		}
		
		return $data;
	}
	
	public function logout()
	{
		$_SESSION['admin'] = false;
	}
}