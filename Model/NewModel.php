<?
require 'Model/Db.php';

class NewModel extends Model{
	public function add($params)
	{
		$bd = new Db;
	
		$params['name'] = htmlspecialchars($params['name']);
		$params['email'] = htmlspecialchars($params['email']);
		$params['content'] = htmlspecialchars($params['content']);
		if($params['name']&&$params['email']&&$params['content']){
			$sql = "INSERT INTO testbeej (NAME, EMAIL, CONTENT) VALUES (?,?,?)";
			$new_id = $bd->execute($sql, [$params['name'], $params['email'], $params['content']]);
			$data['status'] = 'ok';
		} else $data['error'] = 'ошибка добавления';
		//var_dump($data);exit;
		return $data;
	}
	
	public function get($params){
		$bd = new Db;
		$data = $bd->execute('SELECT * FROM testbeej WHERE id = '.intval($params['id']).';');		
		return $data;
	}
	
	public function edit($params)
	{
		$bd = new Db;
	
		$params['name'] = htmlspecialchars($params['name']);
		$params['email'] = htmlspecialchars($params['email']);
		$params['content'] = htmlspecialchars($params['content']);
		$params['finished'] = ($params['finished']=='on' ? 1 : 0);
		$params['edit_by_admin'] = intval($params['edit_by_admin']);
		$params['id'] = intval($params['id']);
		if($params['id'] && $params['name'] && $params['email'] && $params['content']){
			$sql = "UPDATE testbeej SET NAME=?, EMAIL=?, CONTENT=?, FINISHED=?, EDIT_BY_ADMIN=? WHERE ID=?";
			$new_id = $bd->execute($sql, [$params['name'], $params['email'], $params['content'], $params['finished'], $params['edit_by_admin'], $params['id']]);
			$data['status'] = 'ok';
		} else $data['error'] = 'ошибка изменения';
		//var_dump($data);exit;
		$data = $this->get(['id'=>$params['id']]);
		return $data;
	}
}