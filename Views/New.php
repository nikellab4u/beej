<?
//var_dump($params);
/*
- имени пользователя;
- е-mail;
- текста задачи;
*/
if($params['status'] == 'ok') echo "Задача создана";
if($params['error']) echo $params['error'];
?>
<form action="/new" method="POST">
  <div class="form-group">
    <label for="exampleInputEmail1">Имя</label>
    <input name="name" required type="text" class="form-control <?=($params['error']['name']? ' is-invalid' : '')?>" placeholder="Введите имя">
	<div class="invalid-feedback">
	  <?=$params['error']['name']?>
	</div>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input name="email" type="email" required class="form-control <?=($params['error']['password']? ' is-invalid' : '')?>" id="exampleInputEmail1" placeholder="Введите email">
	<div class="invalid-feedback">
	  <?=$params['error']['email']?>
	</div>
  </div>
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Текст задачи</label>
    <textarea name="content" required class="form-control <?=($params['error']['content']? ' is-invalid' : '')?>" id="exampleFormControlTextarea1" rows="3"></textarea>
	<div class="invalid-feedback">
	  <?=$params['error']['content']?>
	</div>
  </div>  
  <button type="submit" class="btn btn-primary">Submit</button>
  
</form>