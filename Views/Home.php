<?
//var_dump($params);
?>
<link href="https://unpkg.com/bootstrap-table@1.15.4/dist/bootstrap-table.min.css" rel="stylesheet">
<script src="https://unpkg.com/bootstrap-table@1.15.4/dist/bootstrap-table.min.js"></script>

<table class="table" id="table" data-pagination="true" data-side-pagination="client" data-page-size="3">
  <thead>
    <tr>
      <th data-field="id">№ п/п</th>
      <th data-field="name"  data-sortable="true">Имя</th>
      <th data-field="email"  data-sortable="true">E-mail</th>
      <th data-field="content">Текст Задачи</th>
	  <th data-field="edited">edit by admin</th>
	  <th data-field="finished"  data-sortable="true">выполнено</th>
    </tr>
  </thead>  
</table>

<script>
  var $table = $('#table')
  $(function() {
    var data = [
	<?
	foreach($params as $data){
		echo "
		{
			'id': '".$data['ID']."',
			'name': '".$data['NAME']."',
			'email': '".$data['EMAIL']."',
			'content': '".$data['CONTENT']."',
			'edited': '".($data['EDIT_BY_ADMIN']?'V':'')."',
			'finished': '".($data['FINISHED']?'V':'')."',
		},
		";
	}
	?>      
      ]
    $table.bootstrapTable({data: data})
  })
<? if($_SESSION['admin']) { ?>  
  $table.on("click-row.bs.table", function (row, $element, field) {
      //console.log(field[0]['firstElementChild'].innerText);//.firstElementChild.innerText
	  document.location.href = '/new/edit/?id = ' + field[0]['firstElementChild'].innerText;
 });
<? } ?> 
</script>