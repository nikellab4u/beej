<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <title>Тест</title>
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <!-- Подключаем Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous" ></script>
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body>		
        <header>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">			  
			  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			  </button>
			  <div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">				
				  <li class="nav-item <?= ($viewName=='Home'?' active' : '') ?>">
					<a class="nav-link " href="/">Список</a>
				  </li>
				  <li class="nav-item <?= ($viewName=='New'?' active' : '') ?>">
					<a class="nav-link" href="/new">Создать задачу</a>
				  </li>
				  <li class="nav-item<?= ($viewName=='Authorize'?' active' : '') ?>">
					<? if(!$_SESSION['admin']) {?>
						<a class="nav-link" href="/authorize">Авторизация</a>
					<? } else {?>
						<a class="nav-link" href="/authorize/logout">Выйти</a>
					<? }?>
				  </li>				  
				</ul>
			  </div>
			</nav>			
        </header>
        <div class="main">
                <?= $body ?>
        </div>		
    </body>
</html>