<?
$params = $params[0];
//var_dump($params);
/*
- имени пользователя;
- е-mail;
- текста задачи;

*/
if($params['status'] == 'ok') echo "Задача изменена";
if($params['error']) echo $params['error'];
?>
<form action="/new/edit" method="POST">
  <div class="form-group">
    <label for="exampleInputEmail1">Имя</label>
    <input name="name" required type="text" class="form-control" value="<?=$params['NAME']?>">	
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input name="email" type="email" required class="form-control" value="<?=$params['EMAIL']?>">	
  </div>
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Текст задачи</label>
    <textarea name="content" required class="form-control <?=($params['error']['content']? ' is-invalid' : '')?>" id="exampleFormControlTextarea1" rows="3"><?=$params['CONTENT']?></textarea>
	<div class="invalid-feedback">
	  <?=$params['error']['content']?>
	</div>
  </div>
  <div class="form-check">
	<input class="form-check-input" type="checkbox" <?=($params['FINISHED'] ? ' checked = "checked" ':'')?> name = "finished" id="defaultCheck1">
	<label class="form-check-label" for="defaultCheck1">
	Выполнено
	</label>
  </div>
	<input name="id" type="hidden" value="<?=$params['ID']?>">
	<input name="edit_by_admin" type="hidden" value="1">
  <button type="submit" class="btn btn-primary">Submit</button>
  
</form>