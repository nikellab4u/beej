<form action="/authorize" method="POST">
  <div class="form-group">
    <label for="exampleInputEmail1">Имя</label>
    <input name="name" required type="text" class="form-control <?=($params['error']['name']? ' is-invalid' : '')?>" placeholder="Введите имя">
	<div class="invalid-feedback">
	  <?=$params['error']['name']?>
	</div>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Пароль</label>
    <input name="password" type="password" required class="form-control <?=($params['error']['password']? ' is-invalid' : '')?>" placeholder="Пароль">
	<div class="invalid-feedback">
	  <?=$params['error']['password']?>
	</div>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
  
</form>