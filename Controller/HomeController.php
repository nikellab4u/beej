<?

class HomeController extends Controller
{
    
	
	function __construct()
	{
		$this->model = new HomeModel();
		//$this->view = new View();
	}
	
	function index()
	{
		$data = $this->model->get_data();				
		return $this->render('Home', $data);
	}    
    
}