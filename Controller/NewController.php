<?

class NewController extends Controller
{
    
	
	function __construct()
	{
		$this->model = new NewModel();
		//$this->view = new View();
	}
	
	function index()
	{
		$data = [];//$this->model->get_data();
		if(isset($_POST['name']) && isset($_POST['email']) && isset($_POST['content'])){//форма добавления
			$data = $this->model->add(['name'=>$_POST['name'], 'email'=>$_POST['email'], 'content'=>$_POST['content']]);
		}
		return $this->render('New', $data);
	}
	
	function edit()
	{
		$data = [];//$this->model->get_data();
		//var_dump($_GET['id_']);
		if(intval($_GET['id_'])&&($_SESSION['admin'])){//форма редактирования открыта
			$data = $this->model->get(['id'=>$_GET['id_']]);
		}
		if(($_SESSION['admin']) && isset($_POST['name']) && isset($_POST['email']) && isset($_POST['content']) && isset($_POST['id'])){//форма редактирования сохранена
			$data = $this->model->edit(['id'=>$_POST['id'], 'name'=>$_POST['name'], 'email'=>$_POST['email'], 'content'=>$_POST['content'], 'finished'=>$_POST['finished'], 'edit_by_admin'=>$_POST['edit_by_admin']]);
		}
		return $this->render('Edit', $data);
	}
    
}