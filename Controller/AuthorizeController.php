<?

class AuthorizeController extends Controller
{
    
	
	function __construct()
	{
		$this->model = new AuthorizeModel();
		//$this->view = new View();
	}
	
	function index()
	{		
		$data = $this->model->get_data();
		//var_dump($_SESSION['admin']=0);
		if($data['authorized']) header('Location: / ');
		if(isset($_POST['name']) && isset($_POST['password'])){//заполнялась форма авторизации, проверим её
			$data = $this->model->authorize(['name'=>$_POST['name'], 'password'=>$_POST['password']]);			
		}		
		return $this->render('Authorize', $data);
	}

	
	function logout()
	{
		$data = $this->model->logout();	
		header('Location: / ');
	}
    
}