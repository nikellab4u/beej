<?php

class Controller 
{
    
    public $layoutFile = 'Views/Layout.php';
    
    public function renderLayout ($body, $viewName) 
    {        
        require 'Views/Layout/Layout.php';
    }
    
    public function render ($viewName, array $params = [])
    {
        
        $viewFile = 'Views/'.$viewName.'.php';
        extract($params);
        ob_start();
        require $viewFile;
        $body = ob_get_clean();
        ob_end_clean();  
		//var_dump($body);
        return $this->renderLayout($body, $viewName);
        
    }
    
}